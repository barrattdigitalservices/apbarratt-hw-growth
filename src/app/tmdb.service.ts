import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MovieCreditsDto, MovieDto } from './models/tmdb/movie-dto';
import { MovieSearchResultDto, SearchPageDto } from './models/tmdb/search-dto';

type PosterWidth = 'w92' | 'w154' | 'w185' | 'w342' | 'w500' | 'w780' | 'original';

@Injectable({
  providedIn: 'root'
})
export class TmdbService {

  private BASE_URL = 'https://api.themoviedb.org/3';
  private API_KEY = 'f31cf959da222122fff6cf2728910327';
  private IMAGE_BASE_URL = 'https://image.tmdb.org/t/p';


  constructor(private http: HttpClient) { }

  public searchForMovie = async (query: string) => {
    return await this.http
      .get<SearchPageDto>(`${this.BASE_URL}/search/movie?api_key=${this.API_KEY}&query=${encodeURI(query)}`)
      .toPromise();
  }

  public getMovie = async (id: number) => {
    return await this.http
      .get<MovieDto>(`${this.BASE_URL}/movie/${id}?api_key=${this.API_KEY}`)
      .toPromise();
  }

  public getGenres = (movie: MovieDto) => {
    return movie.genres.map((genre) => genre.name).join(', ');
  }

  public getMovieCredits = async (id: number) => {
    return await this.http
      .get<MovieCreditsDto>(`${this.BASE_URL}/movie/${id}/credits?api_key=${this.API_KEY}`)
      .toPromise();
  }

  public getMovieDirectors = async (id: number) => {
    const credits = await this.getMovieCredits(id);
    const directors = credits.crew.filter((crewMember) => crewMember.job === 'Director');
    return directors.map((director) => director.name).join(', ');
  }

  public searchForPoster = async (query: string) => {
    const results = (await this.searchForMovie(query)).results;
    if(results.length) {
      return this.getPosterFromResult(results[0]);
    } else {
      return undefined;
    }
  }

  public getPosterFromResult = (result: MovieSearchResultDto, width: PosterWidth = 'w185'): string => {
    return result.poster_path ?
      `${this.IMAGE_BASE_URL}/${width}/${result.poster_path}` :
      '';
  }

  public getPosterThumbnail = (result: MovieSearchResultDto) => {
    return this.getPosterFromResult(result, 'w92');
  }

  public getReleaseDateFromResult = (result: MovieSearchResultDto): Date => {
    return new Date(result.release_date);
  }

  public getYearFromResult = (result: MovieSearchResultDto): number => {
    return this.getReleaseDateFromResult(result).getFullYear();
  }

  public getRatingFromResult = (result: MovieSearchResultDto): number => {
    return result.vote_average;
  }
}
