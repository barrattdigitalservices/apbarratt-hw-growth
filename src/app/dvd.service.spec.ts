import { TestBed } from '@angular/core/testing';

import { DvdService } from './dvd.service';
import { dvdDto } from './models/dvd-dto';

const MOCK_DVDS: dvdDto[] = [
  {
    title: 'Shrek',
    releaseDate: '2001-10-23',
    rating: 7,
    cost: 18.99,
    director: 'Andrew Adamson',
    genre: 'Animation',
  }, {
    title: 'Shrek 2',
    releaseDate: '2004-07-02',
    rating: 8,
    cost: 18.99,
    director: 'Andrew Adamson',
    genre: 'Animation',
  }
];

const MOCK_EXTRA_DVD: dvdDto = {
  title: 'Shrek the Third',
  releaseDate: '2007-05-06',
  rating: 3,
  cost: 4.99,
  director: 'Chris Miller',
  genre: 'Animation',
};

const populateWithMock = () => {
  localStorage['apb-hw-growth--dvds'] = JSON.stringify(MOCK_DVDS);
}

const getLocalDvds = () => {
  return JSON.parse(localStorage['apb-hw-growth--dvds'] ?? '[]') as dvdDto[];
}

describe('DvdService', () => {
  let service: DvdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DvdService);
    localStorage.removeItem('apb-hw-growth--dvds');
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getDvdsRequest', () => {
    it('should return an empty array', async () => {
      const result = await service.getDvdsRequest();
      expect(result).toEqual([]);
    });
    it('should return the mock dvds list', async () => {
      populateWithMock();
      const result = await service.getDvdsRequest();
      expect (result).toEqual(MOCK_DVDS);
    });
  });

  describe('createDvdRequest', () => {
    it('should add a DVD to the localstorage', async () => {
      populateWithMock();
      expect(getLocalDvds().length).toBe(2);
      await service.createDvdRequest(MOCK_EXTRA_DVD);
      expect(getLocalDvds().length).toBe(3);
      expect(getLocalDvds()[2].title).toBe('Shrek the Third');
    });
  });

  describe('loadDvds', () => {
    it('should load empty array', async () => {
      await service.loadDvds();
      expect(service.dvds).toEqual([]);
    });
    it('should load mock DVDs', async () => {
      populateWithMock();
      await service.loadDvds();
      expect(service.dvds).toEqual(MOCK_DVDS);
    });
  });

  describe('createDvd', () => {
    it('should add the DVD to the list in state', async () => {
      expect(service.dvds.length).toBe(0);
      await service.createDvd(MOCK_EXTRA_DVD);
      expect(service.dvds.length).toBe(1);
      expect(service.dvds[0]).toEqual(MOCK_EXTRA_DVD);
    });
  });
});
