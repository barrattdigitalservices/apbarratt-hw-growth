export interface MovieDto {
  budget: number,
  genres: {
    name: string,
  }[],
}

export interface MovieCreditsDto {
  crew: {
    name: string,
    job: string,
  }[],
}