import { Injectable } from '@angular/core';
import { dvdDto } from './models/dvd-dto';

@Injectable({
  providedIn: 'root'
})
export class DvdService {

  public dvds: dvdDto[] = [];
  public loading: boolean = false;
  public saving: boolean = false;

  constructor() {
    this.loadDvds();
  }

  public getDvdsRequest = async (): Promise<dvdDto[]> => {
    //mock server side by getting data from local storage after a short delay.
    const data = JSON.parse(localStorage['apb-hw-growth--dvds'] ?? '[]');
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(data);
      }, 750);
    })
  }

  public createDvdRequest = async (dvd: dvdDto): Promise<void> => {
    //mock serve side by adding Dvd to local storage after a short delay.
    return new Promise((resolve) => {
      setTimeout(() => {
        let data = JSON.parse(localStorage['apb-hw-growth--dvds'] ?? '[]');
        data.push(dvd);
        localStorage['apb-hw-growth--dvds'] = JSON.stringify(data);
        resolve();
      }, 750)
    });
  }

  public loadDvds = async () => {
    this.loading = true;
    this.dvds = await this.getDvdsRequest();
    this.loading = false;
  }

  public createDvd = async (dvd: dvdDto) => {
    this.saving = true;
    await this.createDvdRequest(dvd);
    this.dvds.push(dvd);
    this.saving = false;
  }
}
