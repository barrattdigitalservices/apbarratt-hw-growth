import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialogRef } from '@angular/material/dialog';
import { Subject, throwError } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { DvdService } from '../dvd.service';
import { dvdDto } from '../models/dvd-dto';
import { MovieSearchResultDto } from '../models/tmdb/search-dto';
import { TmdbService } from '../tmdb.service';

@Component({
  selector: 'app-new-dvd',
  templateUrl: './new-dvd.component.html',
  styleUrls: ['./new-dvd.component.scss'],
  encapsulation: ViewEncapsulation.None, // not ideal, but mat-autocomplete being a pain
})
export class NewDvdComponent implements OnInit {

  private searchSubject: Subject<string> = new Subject();

  @Input() public done?: Function;

  public dvd: dvdDto = {
    title: '',
    releaseDate: '',
    rating: 0,
    cost: 0,
    director: '',
    genre: '',
  }

  public suggestions: MovieSearchResultDto[];

  constructor(
    public dialogRef: MatDialogRef<NewDvdComponent>,
    public service: DvdService,
    public tmdb: TmdbService) { }

  private search = async (title: string) => {
    if (title) {
      const results = await this.tmdb.searchForMovie(title);
      this.suggestions = results.results;
    }
  }

  ngOnInit(): void {
    this.searchSubject.pipe(
      debounceTime(500)
    ).subscribe((title: string) => {
      this.search(title);
    });
  }

  public onTitleKeyup = (value: string) => {
    this.searchSubject.next(value);
  }

  public onSuggestionSelected = async (event: MatAutocompleteSelectedEvent) => {
    const selected: MovieSearchResultDto = event.option.value;
    this.dvd.title = selected.title;
    this.dvd.releaseDate = selected.release_date;
    this.dvd.rating = selected.vote_average;
    this.tmdb.getMovie(selected.id).then((movie) => {
      this.dvd.cost = movie.budget;
      this.dvd.genre = this.tmdb.getGenres(movie);
    });
    this.tmdb.getMovieDirectors(selected.id).then((directors) => this.dvd.director = directors);
  }

  public addDVD = () => {
    this.service.createDvd(this.dvd);
    this.dialogRef.close();
  }

}
