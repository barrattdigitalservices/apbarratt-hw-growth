import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { DvdService } from '../dvd.service';
import { NewDvdComponent } from '../new-dvd/new-dvd.component';

@Component({
  selector: 'app-shelf',
  templateUrl: './shelf.component.html',
  styleUrls: ['./shelf.component.scss']
})
export class ShelfComponent implements OnInit {

  constructor(public service: DvdService, public dialogue: MatDialog) { }

  ngOnInit(): void {
  }

  public showCreateForm = () => {
    this.dialogue.open(NewDvdComponent, {
      width: '400px',
    });
  }

}
