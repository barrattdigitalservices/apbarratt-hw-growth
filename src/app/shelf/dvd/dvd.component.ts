import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { dvdDto } from 'src/app/models/dvd-dto';
import { TmdbService } from 'src/app/tmdb.service';

@Component({
  selector: 'app-dvd',
  templateUrl: './dvd.component.html',
  styleUrls: ['./dvd.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DvdComponent implements OnInit {

  @Input() public dvd: dvdDto;

  public poster?: string;

  private get releaseDate(): Date {
    return new Date(this.dvd.releaseDate);
  }

  public get localeReleaseDate(): string {
    return this.releaseDate.toLocaleDateString();
  }

  public get year(): number {
    return this.releaseDate.getFullYear();
  }

  constructor(private tmdbService: TmdbService) { }

  async ngOnInit() {
    this.poster = await this.tmdbService.searchForPoster(this.dvd.title);
  }

}
