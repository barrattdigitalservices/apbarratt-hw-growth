# DVD collection Homework Assignment

The following homework assignment was for a company looking for a Front End Developer.

The brief was really quite simple:

>We are looking to create a basic system to track DVD collections. One of the software engineers is currently busy working on the back-end API functionality and it is your task to write the Angular front-end.
>
>The system will require a basic list of DVDs to be displayed along with an add DVD form. The add form should consist of the following fields:
>
- Title - free text.
- Release Date - date.
- Personal Rating - multiple choice of 1 to 10 stars.
- DVD Cost - currency (GBP).
- Director - free text.
- Genre - free text.
>
>The backend engineer has provided an example JSON file with the payload they expect to see when the add form is submitted:
>
```json
{
    "title": "Shrek",
    "releaseDate": "2001-10-23",
    "rating": 7,
    "cost": 18.99,
    "director": "Andrew Adamson",
    "genre": "Animation"
}
```
>
>As the backend is still being worked on, the solution does not need to be complete, therefore adding new DVD's will not yet need to post to an endpoint, but would ideally still be presented on the list page.
>
>The list page should show columns for title, release date, rating and cost. 
>
>Your submission should focus on functionality, not beauty. You submission should be in the form of a git repository, with instructions on how to run the application. Submit any assumptions made or points of interest along with the finished application.

I felt this to be a fairly easy assignment but something struck me.  I couldn't help but notice that no real API querying functionality could be shown off in the assignment.  And on top of that, I couldn't help but think that the User Experience of entering endless details of DVDs would be an utterly painful waste of the user's time.

As such, I produced the attached exercise, demonstrating an interface that would take the user's input for a film's name and on the fly, pop up with suggested movie titles.

This was done using a free movie database online that happens to provide a free to use API.  Demonstrating backend calls and also providing graphics for each movie allowed me to provide a Netflix style user interface.

Feedback on the assignment was that "I did more than what was asked of me."  It would seem that this was not good feedback.




# ApbarrattHwGrowth

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
